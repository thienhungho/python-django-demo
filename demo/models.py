import uuid
from django.db import models

# Create your models here.
class Type(models.Model):
    name = models.CharField(max_length=200, db_index=True, unique=True)
    active = models.BooleanField("Is Active", default=True)

    def __str__(self):
        return self.name

class Archive(models.Model):
    name = models.CharField(max_length=200, db_index=True, unique=True)
    active = models.BooleanField("Is Active", default=True)

    def __str__(self):
        return self.name

class Demo(models.Model):
    ON = 'ON'
    OFF = 'OFF'
    DATE_CHOICES = (
        (ON, 'On'),
        (OFF, 'Off'),
    )
    charField = models.CharField("Char", max_length=200, db_index=True, unique=True)
    booleanField = models.BooleanField("Boolean", default=True)
    choicesField = models.CharField("Choices",max_length=2,choices=DATE_CHOICES,default=ON)
    integerField = models.IntegerField("Integer", default=0)
    bigIntegerField = models.BigIntegerField("Big Integer", default=0)
    decimaField = models.DecimalField("Decima", decimal_places=2, max_digits=5)
    floatField = models.FloatField("Float")
    textField = models.TextField("Text")
    dateField = models.DateField("Date", auto_now_add=True)
    dateTimeField = models.DateTimeField("Date Time", auto_now_add=True)
    durationField = models.DurationField("Duration")
    emailField = models.EmailField("Email", db_index=True, unique=True)
    slugField = models.SlugField("Slug")
    urlField = models.URLField("Url")
    uuidField = models.UUIDField("UUID", default=uuid.uuid4)
    ipField = models.GenericIPAddressField("Ip", protocol='both')
    fileField = models.FileField("File", upload_to='uploads/file/%Y/%m/%d/')
    imageField = models.ImageField("Image", upload_to='uploads/img/%Y/%m/%d/')
    archives = models.ManyToManyField(Archive)
    type = models.OneToOneField(Type, on_delete=models.CASCADE)

    def __str__(self):
        return self.charField

class ArchiveShip(models.Model):
    archive = models.ForeignKey(Archive, on_delete=models.CASCADE)
    demo = models.ForeignKey(Demo, on_delete=models.CASCADE)
