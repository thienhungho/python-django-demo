from django.contrib import admin

# Register your models here.
from .models import Demo, Archive, Type, ArchiveShip

admin.site.register(Type)
admin.site.register(Demo)
admin.site.register(Archive)
admin.site.register(ArchiveShip)